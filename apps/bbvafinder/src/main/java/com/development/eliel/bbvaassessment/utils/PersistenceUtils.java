package com.development.eliel.bbvaassessment.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import com.development.eliel.bbvaassessment.MapsApplication;

import java.util.HashSet;

/**
 * Created by eliel.development@gmail.com on 3/29/17.
 */

public class PersistenceUtils {
    private SharedPreferences sharedPref;

    private final static String PREF_FILE = "BBVAAssessmentSP";
    public final static String KEY_LAST_REQUEST_DATE = "KEY_LAST_REQUEST_DATE";

    private static PersistenceUtils instance;

    public static PersistenceUtils getInstance(){
        if (instance == null){
            instance = new PersistenceUtils();
        }

        return instance;
    }

    public void init(){
        sharedPref = MapsApplication.getInstance().getApplicationContext().getSharedPreferences(
                PREF_FILE, Context.MODE_PRIVATE);
    }

    /**
     *
     * Persists an Object
     *
     * @param key
     * @param value
     */
    public void persist(String key, Object value){

        if (value instanceof String)
            persist(key, (String) value);
        if (value instanceof Integer)
            persist(key, (int) value);
        if (value instanceof Boolean)
            persist(key, (boolean) value);
        if (value instanceof Float)
            persist(key, (float) value);
        if (value instanceof Long)
            persist(key, (long) value);
        if (value instanceof HashSet)
            persist(key, (HashSet<String>) value);

    }


    /**
     *
     * Persists a String value
     *
     * @param key
     * @param value
     */
    private void persist(String key, String value){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     *
     * Persists an Integer value
     *
     * @param key
     * @param value
     */
    private void persist(String key, int value){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    /**
     *
     * Persists a Boolean value
     *
     * @param key
     * @param value
     */
    private void persist(String key, boolean value){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    /**
     *
     * Persists a Float value
     *
     * @param key
     * @param value
     */
    private void persist(String key, float value){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    /**
     *
     * Persists a Long value
     *
     * @param key
     * @param value
     */
    private void persist(String key, long value){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(key, value);
        editor.apply();
    }


    private void persist(String key, HashSet<String> value){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putStringSet(key, value);
        editor.apply();
    }

    /**
     *
     * Gets a Value giving the Key and the expected Return type
     *
     * @param key
     * @param returnType
     * @return
     */
    public Object retrieve(String key, Class<?> returnType){
        Object objectToRetrieve = null;
        if (returnType == String.class) {
            objectToRetrieve = sharedPref.getString(key, null);
        }
        if (returnType == Integer.class)
            objectToRetrieve = sharedPref.getInt(key,0);
        if (returnType == Boolean.class)
            objectToRetrieve = sharedPref.getBoolean(key,false);
        if (returnType == Float.class)
            objectToRetrieve = sharedPref.getFloat(key,0f);
        if (returnType == Long.class)
            objectToRetrieve = sharedPref.getLong(key, 0L);

        if (returnType == HashSet.class)
            objectToRetrieve = sharedPref.getStringSet(key, null);

        return objectToRetrieve;
    }
}
