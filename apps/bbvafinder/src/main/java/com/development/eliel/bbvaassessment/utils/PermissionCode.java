package com.development.eliel.bbvaassessment.utils;

/**
 * Created by eliel.development@gmail.com on 3/29/17.
 */

public enum PermissionCode {
    ACCESS_FINE_LOCATION_CODE(10102),
    ACCESS_LOCATION (10103),
    ACCESS_COARSE_LOCATION(10104),
    NONE(0)
    ;

    private final int code;

    PermissionCode(int code) {
        this.code = code;
    }

    public int get() {
        return code;
    }
    public static int enumComparator(int code) {
        int nR = NONE.get();
        for(PermissionCode n : values()) {
            if(code ==n.get() ) {
                nR = n.get();
                break;
            } else {
                nR = NONE.get();
                continue;
            }
        }
        return nR;
    }

}
