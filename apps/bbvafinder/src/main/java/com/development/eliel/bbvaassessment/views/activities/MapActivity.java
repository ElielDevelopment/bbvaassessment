package com.development.eliel.bbvaassessment.views.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.development.eliel.assessmentcore.model.PlacesObj;
import com.development.eliel.bbvaassessment.R;
import com.development.eliel.bbvaassessment.presenters.MapsPresenter;
import com.development.eliel.bbvaassessment.adapters.PlacesListAdapter;
import com.development.eliel.bbvaassessment.presenters.impl.MapsPresenterImpl;
import com.development.eliel.bbvaassessment.utils.SecurityUtils;
import com.development.eliel.bbvaassessment.views.ViewEvent;
import com.development.eliel.bbvaassessment.views.base.BaseView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapActivity extends BaseActivity implements OnMapReadyCallback, BaseView, PlacesListAdapter.OnItemClickListener{

    private static final String TAG = MapActivity.class.getSimpleName();
    private static final String LOCATION_KEY = "LOCATION_KEY";
    private static final String LAST_UPDATED_TIME_STRING_KEY = "LAST_UPDATED_TIME_STRING_KEY";
    private static final String CAM_POSITION_KEY = "CAM_POSITION_KEY";


    private MapsPresenter presenter;

    private GoogleMap mMap;
    private SupportMapFragment mapFragment;

    private RecyclerView placesRecycler;

    private BottomSheetBehavior mBottomSheetBehavior;
    private FloatingActionButton fActionButton;


    private Location mCurrentLocation;
    private int mLastUpdateTime = -1;
    private CameraPosition cp;

    private HashMap<Marker, PlacesObj> data = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        createPresenter();
        presenter.register(this);
        updateValuesFromBundle(savedInstanceState);


    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unregister(this);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        presenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (mCurrentLocation != null)
            savedInstanceState.putParcelable(LOCATION_KEY, mCurrentLocation);
        if (mLastUpdateTime > -1)
            savedInstanceState.putInt(LAST_UPDATED_TIME_STRING_KEY, mLastUpdateTime);
        savedInstanceState.putParcelable(CAM_POSITION_KEY, mMap.getCameraPosition());

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        presenter.onMapReady(googleMap);

    }

    @Override
    public void initView() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



        fActionButton = (FloatingActionButton) findViewById(R.id.fab);
        fActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        placesRecycler = (RecyclerView) findViewById(R.id.places_recyclerview);
        placesRecycler.setLayoutManager(new LinearLayoutManager(this));
        placesRecycler.setAdapter(new PlacesListAdapter(this));
        placesRecycler.setHasFixedSize(true);

        mBottomSheetBehavior = BottomSheetBehavior.from(placesRecycler);
        mBottomSheetBehavior.setPeekHeight(0);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });



    }

    @Override
    public void onPresenterEvent(ViewEvent event) {
        switch (event.getEventType()){
            case PRESENT_OBJECT_EVENT_TYPE:{
                switch ((int) event.getModel().get(ViewEvent.KEY_RESOURCE_ID)){
                    case R.id.map:{
                        mMap = (GoogleMap) event.getModel().get(ViewEvent.KEY_ENTRY);

                        mMap.getUiSettings().setMyLocationButtonEnabled(true);

                        try {
                            enableLocation();
                        }catch (SecurityException e){
                            requestPermissionDlg();
                        }

                        mMap.setOnInfoWindowClickListener(
                                new GoogleMap.OnInfoWindowClickListener(){
                                    public void onInfoWindowClick(Marker marker){
                                        PlacesObj item = data.get(marker);
                                        presenter.onItemClick(item);

                                    }
                                }
                        );
                    }break;
                }
            }break;
            case UPDATE_CONTROL_EVENT_TYPE:{
                switch ((int) event.getModel().get(ViewEvent.KEY_RESOURCE_ID)) {
                    case R.id.map: {
                        if (event.getModel().containsKey(ViewEvent.CONTROL_BEHAVIOR_MOVE_MAP_CAMERA_TO_USER)) {
                            enableLocation();
                            mCurrentLocation = (Location) event.getModel().get(ViewEvent.CONTROL_BEHAVIOR_MOVE_MAP_CAMERA_TO_USER);

                            moveCameraToLocation();
                        }
                    }
                    break;
                }
            }break;
            case PRESENT_RESULTSET_EVENT_TYPE:{
                addMarkers((List<PlacesObj>) event.getModel().get(ViewEvent.KEY_ENTRIES));
                ((PlacesListAdapter)placesRecycler.getAdapter()).update((List<PlacesObj>) event.getModel().get(ViewEvent.KEY_ENTRIES));
            }break;
            case WARNING_FLOW_EVENT_TYPE:{
                requestPermissionDlg();
            }break;
            case NAVIGATE_EVENT_TYPE:{
                goToActivity((Class<?>)event.getModel().get(ViewEvent.KEY_NEXT_VIEW),
                        (Bundle) event.getModel().get(ViewEvent.KEY_BUNDLE_EXTRAS));
            }break;
            case SHOW_ERROR_DIALOG_EVENT_TYPE:{
                showMessageDlg((String) event.getModel().get(ViewEvent.KEY_TITLE), (String) event.getModel().get(ViewEvent.KEY_ERROR_MESSAGE));
            }
        }
    }

    @Override
    public void createPresenter() {
        presenter = new MapsPresenterImpl();
    }

    @Override
    public Activity getActivityInstance() {
        return this;
    }


    public void enableLocation() throws SecurityException{
        if (SecurityUtils.permissionGranted(this)) {
            mMap.setMyLocationEnabled(true);
            if (cp != null) {
                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cp));
            }
        }


    }

    private void moveCameraToLocation(){
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), 15);

        mMap.animateCamera(cameraUpdate, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                presenter.onMoveCameraFinish();
            }

            @Override
            public void onCancel() {

            }
        });
    }

    private void addMarkers (List<PlacesObj> places){
        for (final PlacesObj place : places){
            final Double latitude = place.getGeometry().getLocation().getLat();
            final Double longitude = place.getGeometry().getLocation().getLng();
            final LatLng latLng = new LatLng(latitude, longitude);

            Thread thread = new Thread(new Runnable(){
                @Override
                public void run(){
                    URL url ;
                    try {
                        url = new URL(place.getIcon());
                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                        addMarker(bmp, latLng, place);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
            thread.start();



        }

    }

    private void addMarker(final Bitmap icon, final LatLng latLng, final PlacesObj place){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Marker addedMark = mMap.addMarker(new MarkerOptions().position(latLng).title(place.getName()).icon(BitmapDescriptorFactory.fromBitmap(icon)));
                data.put(addedMark, place);
            }
        });
    }


    private void requestPermissionDlg(){
        ArrayList<String> listPerm = SecurityUtils.getPermissionUnaccepted(this);
        String[] arr = new String[listPerm.size()];
        listPerm.toArray(arr);
        if (listPerm.size() > 0) {
            ActivityCompat.requestPermissions(this, arr, SecurityUtils.REQUEST_PERMISSIONS_CODE);
        }
    }


    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet().contains(LOCATION_KEY)) {
                mCurrentLocation = savedInstanceState.getParcelable(LOCATION_KEY);
            }

            if (savedInstanceState.keySet().contains(LAST_UPDATED_TIME_STRING_KEY)) {
                mLastUpdateTime = savedInstanceState.getInt(
                        LAST_UPDATED_TIME_STRING_KEY);
            }

            if (savedInstanceState.keySet().contains(CAM_POSITION_KEY)){
                cp = savedInstanceState.getParcelable(CAM_POSITION_KEY);
            }

        }
    }


    @Override
    public void onItemClick(PlacesObj item) {
        presenter.onItemClick(item);
    }
}
