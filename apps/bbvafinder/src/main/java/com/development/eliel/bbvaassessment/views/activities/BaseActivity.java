package com.development.eliel.bbvaassessment.views.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.development.eliel.bbvaassessment.R;

/**
 * Created by eliel.development@gmail.com on 3/29/17.
 *
 * The Base Activity class from which every Activity of the application
 * must extends in order to inherit its behavior
 *
 */

public class BaseActivity extends AppCompatActivity {

    public AlertDialog alertDialog;

    /**
     *
     * Launch an {@link Intent} to start a nre Activity
     *
     * @param activity The new Activity {@link Class}
     * @param extras The {@link Bundle} extras
     */
    public void goToActivity(Class<?> activity, Bundle extras) {
        Intent intent = new Intent(this, activity);
        intent.putExtras(extras);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.fade_out);

    }

    /**
     *
     * Shows an {@link AlertDialog}
     *
     * @param title the {@link AlertDialog} Title
     * @param message the {@link AlertDialog} Message
     */
    public void showMessageDlg(String title, String message) {
        alertDialog = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        hideMessageDialog();
                    }
                })
                .show();
    }

    /**
     *
     * Dismiss the {@link AlertDialog}
     *
     */
    public void hideMessageDialog() {
        if (alertDialog != null)
            alertDialog.dismiss();
    }

}
