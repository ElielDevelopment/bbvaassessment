package com.development.eliel.bbvaassessment.presenters;

import android.location.Location;
import android.os.Bundle;

import com.development.eliel.assessmentcore.model.PlacesObj;
import com.development.eliel.bbvaassessment.presenters.base.BasePresenter;
import com.google.android.gms.maps.GoogleMap;

import java.util.List;

/**
 * Created by eliel.development@gmail.com on 3/28/17.
 */

public interface MapsPresenter extends BasePresenter{
    void onMapReady(GoogleMap googleMap);
    void onMoveCameraFinish();
    void onItemClick(PlacesObj item);
}
