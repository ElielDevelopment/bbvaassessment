package com.development.eliel.bbvaassessment.views;

/**
 * Created by eliel.development@gmail.com on 3/28/17.
 */

public enum ViewEventType {
    SHOW_ERROR_DIALOG_EVENT_TYPE (-40),
    CLOSE_ERROR_DIALOG_EVENT_TYPE (-30),
    SHOW_CONFIRM_DIALOG_EVENT_TYPE (-20),
    HIDE_CONFIRM_DIALOG_EVENT_TYPE (-10),
    NAVIGATE_EVENT_TYPE (0),
    NAVIGATE_FRAGMENT_EVENT_TYPE (10),
    OPENAPP_EVENT_TYPE (20),
    SHOW_PROGRESS_EVENT_TYPE (30),
    HIDE_PROGRESS_EVENT_TYPE (40),
    EMPTYSET_EVENT_TYPE (50),
    NETWORK_ERROR (60),
    SHOW_LAYOUT_ELEMENT (70),
    HIDE_LAYOUT_ELEMENT (80),
    PRESENT_OBJECT_EVENT_TYPE (90),
    PRESENT_RESULTSET_EVENT_TYPE (100),
    ENABLE_CONTROL_EVENT_TYPE (110),
    DISABLE_CONTROL_EVENT_TYPE (120),
    UPDATE_CONTROL_EVENT_TYPE (130),
    REFRESH_VIEW (140),
    SHOW_TOAST_MESSAGE (150),
    SHOW_DIALOG_EVENT_TYPE (160),
    HIDE_DIALOG_EVENT_TYPE (170),
    HIDE_KEYBOARD_EVENT_TYPE (180),
    ERROR_FLOW_EVENT_TYPE (190),
    WARNING_FLOW_EVENT_TYPE (200)
    ;


    private final int number;

    private ViewEventType(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
}
