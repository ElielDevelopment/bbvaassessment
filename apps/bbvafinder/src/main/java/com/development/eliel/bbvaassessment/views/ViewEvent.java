package com.development.eliel.bbvaassessment.views;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by eliel.development@gmail.com on 3/28/17.
 */

public class ViewEvent {

    public static final String KEY_ENTRIES = "entries";
    public static final String KEY_ENTRY = "entry";
    public static final String KEY_BUNDLE_EXTRAS = "bundle_extras";
    public static final String KEY_TITLE = "title";
    public static final String KEY_RESOURCE_ID = "resource_id";
    public static final String KEY_ERROR_MESSAGE = "error_message";
    public static final String KEY_NEXT_VIEW = "next_view";
    public static final String KEY_APP = "app";
    public static final String KEY_URI = "uri";

    //=== This Keys has to be used with the UPDATE_CONTROL_EVENT_TYPE ViewEventType

    // begin region control state

    //Values could be true / false
    public static final String CONTROL_STATE_ENABLED = "CONTROL_STATE_ENABLED";
    //Values could be View.VISIBLE, View..INVISIBLE, View.GONE
    public static final String CONTROL_STATE_VISIBILITY = "CONTROL_STATE_VISIBILITY";
    //Values could be true / false
    public static final String CONTROL_STATE_FOCUSED = "CONTROL_STATE_FOCUSED";
    //Values could be true / false
    public static final String CONTROL_STATE_TOUCHABLE = "CONTROL_STATE_TOUCHABLE";
    //Values could be true / false
    public static final String CONTROL_STATE_CLICKABLE = "CONTROL_STATE_CLICKABLE";
    //Values could be true / false for controls with show() & hide() methods
    public static final String CONTROL_STATE_SHOWED = "CONTROL_STATE_SHOWED";
    //Values could be true / false
    public static final String CONTROL_STATE_LOCKED = "CONTROL_STATE_LOCKED";
    //Values could be an int progress
    public static final String CONTROL_STATE_PROGRESS = "CONTROL_STATE_PROGRESS";


    // end region control state

    // begin region control look & feel

    //Values should be a int drawable id
    public static final String CONTROL_LOOK_DRAWABLE = "CONTROL_LOOK_DRAWABLE";

    //Values should be a int color id
    public static final String CONTROL_LOOK_COLOR = "CONTROL_LOOK_COLOR";

    //Values should be a int string id
    public static final String CONTROL_LOOK_TEXT = "CONTROL_LOOK_TEXT";

    //Values should be a int string id
    public static final String CONTROL_LOOK_TEXT_COLOR = "CONTROL_LOOK_TEXT_COLOR";

    // end region control look & feel

    // begin region control behavior
    //Values could be the next item Object, position or even null
    public static final String CONTROL_BEHAVIOR_NEXT_ITEM = "CONTROL_BEHAVIOR_NEXT_ITEM";

    //Values could be the previous item Object, position or even null
    public static final String CONTROL_BEHAVIOR_PREV_ITEM = "CONTROL_BEHAVIOR_PREV_ITEM";

    //Values could be the main item Object, position or even null
    public static final String CONTROL_BEHAVIOR_RESET_TO_MAIN = "CONTROL_BEHAVIOR_RESET_TO_MAIN";

    //Value Location: showing the user Location on a map
    public static final String CONTROL_BEHAVIOR_SHOW_USER_LOCATION = "CONTROL_BEHAVIOR_SHOW_USER_LOCATION";

    //Value true / false
    public static final String CONTROL_BEHAVIOR_MOVE_MAP_CAMERA_TO_USER = "CONTROL_BEHAVIOR_MOVE_MAP_CAMERA_TO_USER";

    //Values int Direction od animation
    public static final String CONTROL_BEHAVIOR_ANIMATE_TO = "CONTROL_BEHAVIOR_ANIMATE_TO";

    //Values true/false if the control should animate
    public static final String CONTROL_BEHAVIOR_ANIMATE = "CONTROL_BEHAVIOR_ANIMATE";


    // end region control behavior

    private ViewEventType eventType;
    private Map<String,Object> model;

    public ViewEvent (ViewEventType eventType){
        this.eventType = eventType;
    }



    public ViewEventType getEventType() {
        return eventType;
    }

    public void setEventType(ViewEventType eventType) {
        this.eventType = eventType;
    }

    public Map<String, Object> getModel() {
        if (model == null)
            model = new HashMap<String, Object>();
        return model;
    }

    public Object getModel(String key) {
        return model.get(key);
    }

    public void setModel(Map<String, Object> model) {
        this.model = model;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ViewEvent{");
        sb.append("eventType=").append(eventType);
        sb.append(", model=").append(model);
        sb.append('}');
        return sb.toString();
    }
}
