package com.development.eliel.bbvaassessment.utils;

import android.content.Context;
import android.location.Location;

import com.development.eliel.bbvaassessment.MapsApplication;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

/**
 * Created by eliel.development@gmail.com on 3/29/17.
 */

public class LocationUtils {

    GoogleApiClient mGoogleApiClient;

    private static LocationUtils instance;

    public static LocationUtils getInstance(){
        if (instance == null){
            instance = new LocationUtils();
        }

        return instance;
    }

    public GoogleApiClient getmGoogleApiClient() {
        return mGoogleApiClient;
    }

    public void createGoogleApiClient(GoogleApiClient.ConnectionCallbacks callbacks, GoogleApiClient.OnConnectionFailedListener connectionFailedListener) {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(MapsApplication.getInstance().getApplicationContext())
                    .addConnectionCallbacks(callbacks)
                    .addOnConnectionFailedListener(connectionFailedListener)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    public void destroyClient(){
        mGoogleApiClient = null;
    }


    public void connect(){
        mGoogleApiClient.connect();
    }

    public void disconnect(){
        mGoogleApiClient.disconnect();
    }

    public Location getLastLocation() throws SecurityException{
        return LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
    }


}
