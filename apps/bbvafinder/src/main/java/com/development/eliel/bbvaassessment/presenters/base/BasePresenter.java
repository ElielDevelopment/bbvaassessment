package com.development.eliel.bbvaassessment.presenters.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.development.eliel.bbvaassessment.views.ViewEvent;
import com.development.eliel.bbvaassessment.views.base.BaseView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by eliel.development@gmail.com on 3/28/17.
 *
 * Base Presenter Interface to determine the Methods for any Presenter Object
 *
 */

public interface BasePresenter {
    /**
     *
     * This method allows to know when {@link android.app.Activity#onCreate(Bundle)} occurs
     *
     * @param savedInstanceState
     */
    void onCreate(@Nullable final Bundle savedInstanceState);

    /**
     *
     * This method allows to know when {@link android.support.v4.app.Fragment#onCreateView(LayoutInflater, ViewGroup, Bundle)} occurs
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     */
    void onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    /**
     *
     * This method allows to know when {@link android.support.v4.app.Fragment#onViewCreated(View, Bundle)} occurs
     *
     * @param view
     * @param savedInstanceState
     */
    void onViewCreated(View view, @Nullable Bundle savedInstanceState);

    /**
     *
     * Register a {@link BaseView} implementation to the {@link BasePresenter} implementation
     *
     * @param view The {@link BaseView} to be registered
     */
    void register(BaseView view);

    /**
     *
     * Unregister a {@link BaseView} implementation to the {@link BasePresenter} implementation
     *
     * @param view The {@link BaseView} to be unregistered
     */
    void unregister(BaseView view);

    /**
     *
     * This method allows to know when {@link Activity#onStart()} or {@link Fragment#onStart()}  occurs
     *
     */
    void onStart();

    /**
     *
     * This method allows to know when {@link Activity#onResume()} or {@link Fragment#onResume()}  occurs
     *
     */
    void onResume();

    /**
     *
     * This method allows to know when {@link Activity#onPause()} or {@link Fragment#onPause()}  occurs
     *
     */
    void onPause();

    /**
     *
     * This method allows to know when {@link Activity#onStop()} or {@link Fragment#onStop()}  occurs
     *
     */
    void onStop();

    /**
     *
     * This method allows to know when {@link Activity#onSaveInstanceState(Bundle)} or {@link Fragment#onSaveInstanceState(Bundle)} occurs
     *
     * @param outState
     */
    void onSaveInstanceState(@NonNull final Bundle outState);

    /**
     *
     * This method allows to know when {@link Activity#onDestroy()} or {@link Fragment#onDestroy()} occurs
     *
     */
    void onDestroy();

    /**
     *
     * This method allows to know when {@link Activity#onActivityResult(int, int, Intent)} occurs
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data);

    /**
     *
     * This method allows to know when {@link Activity#onRequestPermissionsResult(int, String[], int[])} occurs
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults);

    /**
     *
     * Gets the Registered {@link BaseView}
     *
     * @return The {@link BaseView} instance
     */
    BaseView getView();

    /**
     *
     * Notifies to the registered {@link BaseView} about
     * UI events
     *
     * @param event
     */
    void notifyView(ViewEvent event);

    /**
     *
     * Related Data to the event
     *
     * @param key The key of the event
     * @return
     */
    Object getEventExtras(String key);

    /**
     *
     * Related Data to the event
     *
     * @return A Key, Value Map data
     */
    Map<String, Object> getEventExtras();


}
