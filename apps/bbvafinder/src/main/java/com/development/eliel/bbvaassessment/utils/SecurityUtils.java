package com.development.eliel.bbvaassessment.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by eliel.development@gmail.com on 3/29/17.
 */

public class SecurityUtils {

    public static int REQUEST_PERMISSIONS_CODE = 600;

    public static boolean permissionGranted(Activity activity) throws SecurityException{
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            throw new SecurityException("Must Grant Permissions");
        }

        return true;
    }

    public static ArrayList<String> getPermissionUnaccepted(Context context) {
        PermissionType[] permissionTypes = PermissionType.values();
        PermissionCode[] permissionCodes = PermissionCode.values();
        ArrayList<String> listPerm = new ArrayList<>();
        for(int i = 0; i < permissionTypes.length && i < permissionCodes.length; i++) {
            if (ContextCompat.checkSelfPermission(context,
                    permissionTypes[i].get())
                    != PackageManager.PERMISSION_GRANTED) {
                listPerm.add(permissionTypes[i].get());
            }
        }

        return listPerm;
    }




}
