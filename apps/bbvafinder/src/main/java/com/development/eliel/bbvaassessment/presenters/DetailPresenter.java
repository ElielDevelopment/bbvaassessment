package com.development.eliel.bbvaassessment.presenters;

import android.os.Bundle;

import com.development.eliel.assessmentcore.model.PlacesObj;
import com.development.eliel.bbvaassessment.presenters.base.BasePresenter;

/**
 * Created by eliel.development@gmail.com on 3/29/17.
 */

public interface DetailPresenter extends BasePresenter {

    void onExtrasReceived(Bundle extras);

    void onNavigateButtonClicked();

}
