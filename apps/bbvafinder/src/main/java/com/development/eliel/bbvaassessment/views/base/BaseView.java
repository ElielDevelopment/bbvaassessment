package com.development.eliel.bbvaassessment.views.base;

import android.app.Activity;

import com.development.eliel.bbvaassessment.views.ViewEvent;

/**
 * Created by eliel.development@gmail.com on 3/28/17.
 */

public interface BaseView {

    /**
     * Initializes View elements
     */
    void initView();

    /**
     * Hanles Presenter's Notification of View Events
     * @param event
     */
    void onPresenterEvent(ViewEvent event);

    /**
     * Creates view's presenter instance
     */
    void createPresenter();

    /**
     *
     * Gets the Activity Instance
     *
     */

    Activity getActivityInstance();


}
