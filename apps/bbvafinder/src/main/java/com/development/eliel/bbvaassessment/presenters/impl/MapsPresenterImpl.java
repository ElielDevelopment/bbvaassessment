package com.development.eliel.bbvaassessment.presenters.impl;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.development.eliel.assessmentcore.api.client.Callback;
import com.development.eliel.assessmentcore.api.client.Error;
import com.development.eliel.assessmentcore.api.client.Response;
import com.development.eliel.assessmentcore.api.request.RequestBBVAPlaces;
import com.development.eliel.assessmentcore.api.responses.SearchPlacesResponse;
import com.development.eliel.assessmentcore.model.PlacesObj;
import com.development.eliel.bbvaassessment.MapsApplication;
import com.development.eliel.bbvaassessment.R;
import com.development.eliel.bbvaassessment.presenters.MapsPresenter;
import com.development.eliel.bbvaassessment.presenters.base.BasePresenterImpl;
import com.development.eliel.bbvaassessment.utils.LocationUtils;
import com.development.eliel.bbvaassessment.utils.PermissionType;
import com.development.eliel.bbvaassessment.utils.PersistenceUtils;
import com.development.eliel.bbvaassessment.utils.SecurityUtils;
import com.development.eliel.bbvaassessment.views.ViewEvent;
import com.development.eliel.bbvaassessment.views.ViewEventType;
import com.development.eliel.bbvaassessment.views.activities.PlaceDetailActivity;
import com.development.eliel.bbvaassessment.views.base.BaseView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.development.eliel.bbvaassessment.utils.SecurityUtils.REQUEST_PERMISSIONS_CODE;

/**
 * Created by eliel.development@gmail.com on 3/28/17.
 */

public class MapsPresenterImpl extends BasePresenterImpl implements MapsPresenter,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String TAG = MapsPresenter.class.getSimpleName();

    private List<PlacesObj> places;

    @Override
    public void onStart() {
        if (!LocationUtils.getInstance().getmGoogleApiClient().isConnected()){
            LocationUtils.getInstance().connect();
        }

        super.onStart();
    }

    @Override
    public void onStop() {
        if (LocationUtils.getInstance().getmGoogleApiClient().isConnected()){
            LocationUtils.getInstance().disconnect();
        }
        super.onStop();
    }

    @Override
    public void register(BaseView view) {
        LocationUtils.getInstance().createGoogleApiClient(this, this);
        super.register(view);
    }

    @Override
    public void unregister(BaseView view) {
        LocationUtils.getInstance().destroyClient();
        super.unregister(view);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        boolean shouldEnableLocation = true;

        if (requestCode == REQUEST_PERMISSIONS_CODE){
            for (int i = 0, len = permissions.length; i < len; i++) {
                final String permission = permissions[i];
                if (((permission.contentEquals(PermissionType.ACCESS_COARSE_LOCATION.get())) && (grantResults[i]== PackageManager.PERMISSION_DENIED))
                    || ((permission.contentEquals(PermissionType.ACCESS_FINE_LOCATION.get())) && (grantResults[i]== PackageManager.PERMISSION_DENIED))) {

                    shouldEnableLocation = false;

                    boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(getView().getActivityInstance(), permission);
                    if (!showRationale) {

                        if (grantResults[i] == PackageManager.PERMISSION_DENIED) {

                            shouldEnableLocation = false;

                            final Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.setData(Uri.parse("package:" + getView().getActivityInstance().getPackageName()));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                            getView().getActivityInstance().startActivity(intent);
                        }
                    }
                }
            }

            if (shouldEnableLocation)
                updateUserPosition();
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        ViewEvent viewEvent = new ViewEvent(ViewEventType.PRESENT_OBJECT_EVENT_TYPE);
        viewEvent.getModel().put(ViewEvent.KEY_RESOURCE_ID, R.id.map);
        viewEvent.getModel().put(ViewEvent.KEY_ENTRY, googleMap);
        notifyView(viewEvent);
    }



    @Override
    public void onMoveCameraFinish() {

        if (mustRequestPlaces()) {

            Location userLocation = LocationUtils.getInstance().getLastLocation();
            Log.d(TAG, "Will Request places at "+ System.currentTimeMillis());

            persistenceUtils().persist(PersistenceUtils.KEY_LAST_REQUEST_DATE, System.currentTimeMillis());

            if (!isOnline()){
                ViewEvent viewEvent = new ViewEvent(ViewEventType.SHOW_ERROR_DIALOG_EVENT_TYPE);
                viewEvent.getModel().put(ViewEvent.KEY_TITLE, getStringFromResources(R.string.title_error));
                viewEvent.getModel().put(ViewEvent.KEY_ERROR_MESSAGE, getStringFromResources(R.string.network_error));
                notifyView(viewEvent);

                return;
            }

            RequestBBVAPlaces request = new RequestBBVAPlaces()
                    .withQuery("BBVA")
                    .withLat(userLocation.getLatitude())
                    .withLng(userLocation.getLongitude())
                    .withRadius(10000)
                    .setApiKey(MapsApplication.getInstance().getApplicationContext().getResources().getString(R.string.google_maps_api_key));

            client().maps().getBBVAPlaces(request, new Callback<SearchPlacesResponse>() {
                @Override
                public void onSuccess(Response<SearchPlacesResponse> response) {

                    if (response.body().getError_message()!= null){
                        ViewEvent viewEvent = new ViewEvent(ViewEventType.SHOW_ERROR_DIALOG_EVENT_TYPE);
                        viewEvent.getModel().put(ViewEvent.KEY_TITLE, getStringFromResources(R.string.title_error));
                        viewEvent.getModel().put(ViewEvent.KEY_ERROR_MESSAGE, response.body().getError_message());
                        notifyView(viewEvent);
                        return;
                    }

                    places = response.body().getResults();
                    ViewEvent viewEvent = new ViewEvent(ViewEventType.PRESENT_RESULTSET_EVENT_TYPE);
                    viewEvent.getModel().put(ViewEvent.KEY_ENTRIES, places);
                    notifyView(viewEvent);
                }

                @Override
                public void onHttpError(Error error) {
                    ViewEvent viewEvent = new ViewEvent(ViewEventType.SHOW_ERROR_DIALOG_EVENT_TYPE);
                    viewEvent.getModel().put(ViewEvent.KEY_TITLE, getStringFromResources(R.string.title_error));
                    viewEvent.getModel().put(ViewEvent.KEY_ERROR_MESSAGE, error.getMessage());
                    notifyView(viewEvent);
                }

                @Override
                public void onFailure(Exception e) {
                    ViewEvent viewEvent = new ViewEvent(ViewEventType.SHOW_ERROR_DIALOG_EVENT_TYPE);
                    viewEvent.getModel().put(ViewEvent.KEY_TITLE, getStringFromResources(R.string.title_error));
                    viewEvent.getModel().put(ViewEvent.KEY_ERROR_MESSAGE, e.getMessage());
                    notifyView(viewEvent);
                }
            });
        }else{
            ViewEvent viewEvent = new ViewEvent(ViewEventType.PRESENT_RESULTSET_EVENT_TYPE);
            viewEvent.getModel().put(ViewEvent.KEY_ENTRIES, places);
            notifyView(viewEvent);
        }
    }

    @Override
    public void onItemClick(PlacesObj item) {

        Bundle bundle = new Bundle();
        bundle.putSerializable(PlaceDetailActivity.KEY_PLACE, item);


        ViewEvent viewEvent = new ViewEvent(ViewEventType.NAVIGATE_EVENT_TYPE);
        viewEvent.getModel().put(ViewEvent.KEY_NEXT_VIEW, PlaceDetailActivity.class);
        viewEvent.getModel().put(ViewEvent.KEY_BUNDLE_EXTRAS, bundle);
        notifyView(viewEvent);
    }

    @Override
    public void onConnected(Bundle bundle) {
        updateUserPosition();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        updateUserPosition(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


    private void updateUserPosition(){
        try {
            if (SecurityUtils.permissionGranted(getView().getActivityInstance())) {
                updateUserPosition(LocationUtils.getInstance().getLastLocation());
            }
        }catch (SecurityException e){
            ViewEvent viewEvent = new ViewEvent(ViewEventType.WARNING_FLOW_EVENT_TYPE);
            notifyView(viewEvent);
        }
    }

    private void updateUserPosition(Location location){
        try {
            if (SecurityUtils.permissionGranted(getView().getActivityInstance())) {
                ViewEvent viewEvent = new ViewEvent(ViewEventType.UPDATE_CONTROL_EVENT_TYPE);
                viewEvent.getModel().put(ViewEvent.KEY_RESOURCE_ID, R.id.map);
                viewEvent.getModel().put(ViewEvent.CONTROL_BEHAVIOR_MOVE_MAP_CAMERA_TO_USER, location);
                notifyView(viewEvent);
            }
        }catch (SecurityException e){
            ViewEvent viewEvent = new ViewEvent(ViewEventType.WARNING_FLOW_EVENT_TYPE);
            notifyView(viewEvent);
        }
    }


    private boolean mustRequestPlaces(){
        long currentTime = System.currentTimeMillis();
        long lastUpdated = (long) persistenceUtils().retrieve(PersistenceUtils.KEY_LAST_REQUEST_DATE, Long.class);
        return ((currentTime - lastUpdated) >= 300000) || (places == null);

    }

    private String getStringFromResources(int stringId){
        return MapsApplication.getInstance().getApplicationContext().getString(stringId);
    }
}
