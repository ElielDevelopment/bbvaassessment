package com.development.eliel.bbvaassessment.presenters.base;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.development.eliel.assessmentcore.api.ApiClient;
import com.development.eliel.bbvaassessment.MapsApplication;
import com.development.eliel.bbvaassessment.utils.PersistenceUtils;
import com.development.eliel.bbvaassessment.views.ViewEvent;
import com.development.eliel.bbvaassessment.views.base.BaseView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by eliel.development@gmail.com on 3/29/17.
 *
 * Base Presenter Class from which other Presenters will be derived
 *
 */

public class BasePresenterImpl implements BasePresenter {
    private BaseView view;
    private Map<String, Object> eventExtras;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

    }

    @Override
    public void onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

    }

    @Override
    public void register(BaseView view) {
        this.view = view;
        this.view.initView();
    }

    @Override
    public void unregister(BaseView view) {
        if (this.view == view)
            this.view = null;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

    }

    @Override
    public void notifyView(ViewEvent viewEvent) {
        view.onPresenterEvent(viewEvent);
    }

    @Override
    public Object getEventExtras(String key) {
        return null;
    }

    @Override
    public Map<String, Object> getEventExtras() {
        if (eventExtras == null)
            eventExtras = new HashMap<String, Object>();
        return eventExtras;
    }

    @Override
    public BaseView getView() {
        return this.view;
    }

    public ApiClient client(){
        return MapsApplication.getInstance().client();
    }

    public PersistenceUtils persistenceUtils(){
        return MapsApplication.getInstance().persistence();
    }

    /**
     *
     * Verifies the Network status of the Device
     *
     * @return {@link Boolean} if the device is Online
     */
    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                MapsApplication.getInstance().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }
}
