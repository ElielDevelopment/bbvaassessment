package com.development.eliel.bbvaassessment.views.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.development.eliel.assessmentcore.model.PlacesObj;
import com.development.eliel.bbvaassessment.MapsApplication;
import com.development.eliel.bbvaassessment.R;
import com.development.eliel.bbvaassessment.presenters.DetailPresenter;
import com.development.eliel.bbvaassessment.presenters.impl.DetailPresenterImpl;
import com.development.eliel.bbvaassessment.views.ViewEvent;
import com.development.eliel.bbvaassessment.views.ViewEventType;
import com.development.eliel.bbvaassessment.views.base.BaseView;
import com.squareup.picasso.Picasso;

public class PlaceDetailActivity extends BaseActivity implements BaseView, View.OnClickListener {



    public static final String TAG = PlaceDetailActivity.class.getSimpleName();
    public static final String KEY_PLACE = "KEY_PLACE";
    private static final String BASE_PLACES_URL = "https://maps.googleapis.com/maps/api/place/photo?photoreference=";

    private DetailPresenter presenter;

    private ImageView placePhoto;
    private TextView placeName;
    private TextView placeAddress;
    private RatingBar placeRating;
    private FloatingActionButton fbNavigateTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_detail);
        createPresenter();
        presenter.register(this);
        presenter.onExtrasReceived(getIntent().getExtras());
    }

    @Override
    public void initView() {
        placePhoto = (ImageView) findViewById(R.id.placePhoto);
        placeName = (TextView) findViewById(R.id.placeName);
        placeAddress = (TextView) findViewById(R.id.placeAddress);
        placeRating = (RatingBar) findViewById(R.id.placeRating);
        fbNavigateTo = (FloatingActionButton) findViewById(R.id.fabNavigate);
        fbNavigateTo.setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    @Override
    public void onPresenterEvent(ViewEvent event) {
        switch (event.getEventType()){
            case OPENAPP_EVENT_TYPE:{
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, (Uri) event.getModel().get(ViewEvent.KEY_URI));
                mapIntent.setPackage((String) event.getModel().get(ViewEvent.KEY_APP));
                startActivity(mapIntent);
            }break;
            case PRESENT_OBJECT_EVENT_TYPE:{
                switch ((int) event.getModel().get(ViewEvent.KEY_RESOURCE_ID)){
                    case R.id.place_detail_container:{
                        PlacesObj placesObj = (PlacesObj) event.getModel().get(ViewEvent.KEY_ENTRY);
                        placeName.setText(placesObj.getName());
                        placeAddress.setText(placesObj.getFormatted_address());
                        if (placesObj.getRating() != null) {

                            if (placesObj.getRating() instanceof Double)
                                placeRating.setNumStars(((Double) placesObj.getRating()).intValue());
                            else
                                placeRating.setNumStars((Integer) placesObj.getRating());
                        }else
                            placeRating.setNumStars(0);
                        if(placesObj.getPhotos() != null) {

                            Picasso.with(MapsApplication.getInstance().getApplicationContext())
                                    .load(BASE_PLACES_URL + placesObj.getPhotos().get(0).getPhoto_reference() + getPhotosParams())
                                    .placeholder(R.drawable.promo_item_placeholder)
                                    .error(R.drawable.promo_item_placeholder)
                                    .fit().centerCrop()
                                    .into(placePhoto);
                        }

                    }break;
                }
            }
        }
    }

    @Override
    public void createPresenter() {
        presenter = new DetailPresenterImpl();
    }

    @Override
    public Activity getActivityInstance() {
        return null;
    }

    @Override
    public void onClick(View view) {
        presenter.onNavigateButtonClicked();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public String getPhotosParams(){
        String apiKey = this.getResources().getString(R.string.google_maps_api_key);
        return "&key="+apiKey+"&maxwidth=400";
    }
}
