package com.development.eliel.bbvaassessment;

import android.app.Application;

import com.development.eliel.assessmentcore.api.ApiClient;
import com.development.eliel.bbvaassessment.utils.PersistenceUtils;

/**
 * Created by eliel.development@gmail.com on 3/28/17.
 */

public class MapsApplication extends Application {
    private static MapsApplication instance;

    private static ApiClient apiClient;

    public static MapsApplication getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        apiClient = ApiClient.getInstance();
        PersistenceUtils.getInstance().init();
    }

    public ApiClient client(){
        return apiClient;
    }

    public PersistenceUtils persistence(){
        return PersistenceUtils.getInstance();
    }
}
