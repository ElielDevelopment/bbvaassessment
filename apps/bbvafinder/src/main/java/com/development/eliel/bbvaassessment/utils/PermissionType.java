package com.development.eliel.bbvaassessment.utils;

/**
 * Created by eliel.development@gmail.com on 3/29/17.
 */

public enum PermissionType {
    ACCESS_FINE_LOCATION("android.permission.ACCESS_FINE_LOCATION"),
    ACCESS_COARSE_LOCATION("android.permission.ACCESS_COARSE_LOCATION"),
    ;

    private final String permission;

    private PermissionType(final String permission) {
        this.permission = permission;
    }

    @Override
    public String toString() {
        return permission;
    }

    public String get() {
        return permission;
    }
}
