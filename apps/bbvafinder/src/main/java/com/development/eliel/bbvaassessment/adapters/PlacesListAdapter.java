package com.development.eliel.bbvaassessment.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.development.eliel.assessmentcore.model.PlacesObj;
import com.development.eliel.bbvaassessment.MapsApplication;
import com.development.eliel.bbvaassessment.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by eliel.development@gmail.com on 3/29/17.
 *
 * An Adapter to present {@link PlacesObj} objects on a {@link RecyclerView}
 *
 */

public class PlacesListAdapter extends RecyclerView.Adapter<PlacesListAdapter.ViewHolder> {

    private static final String BASE_PLACES_PHOTOS_URL = "https://maps.googleapis.com/maps/api/place/photo?photoreference=";
    private List<PlacesObj> items;

    private OnItemClickListener listener;

    public PlacesListAdapter(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.places_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final PlacesObj item = items.get(position);

        holder.itemTitle.setText(item.getName());
        if (item.getFormatted_address() != null)
            holder.itemDescription.setText(Html.fromHtml(item.getFormatted_address()));
        else
            holder.itemDescription.setText("");

        if(item.getPhotos() != null) {

            //Log.d("Adapter" ,position + "->" + item.getFormatted_address() + " : " + BASE_PLACES_PHOTOS_URL + item.getPhotos().get(0).getPhoto_reference());

            Picasso.with(MapsApplication.getInstance().getApplicationContext())
                    .load(BASE_PLACES_PHOTOS_URL + item.getPhotos().get(0).getPhoto_reference() + getPhotosParams())
                    .placeholder(R.drawable.promo_item_placeholder)
                    .error(R.drawable.promo_item_placeholder)
                    .fit().centerCrop()
                    .into(holder.itemImage);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(item);
            }
        });




    }

    @Override
    public int getItemCount() {
        return (items != null) ? items.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView itemImage;
        private TextView itemTitle;
        private TextView itemDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            itemImage = (ImageView) itemView.findViewById(R.id.itemImage);
            itemTitle = (TextView) itemView.findViewById(R.id.itemTitle);
            itemDescription = (TextView) itemView.findViewById(R.id.itemDescription);

        }
    }

    /**
     *
     * Interface to know when an item was clicked
     *
     */
    public interface OnItemClickListener {
        void onItemClick(PlacesObj item);
    }

    /**
     *
     * Updates the adapter items and notify the changes to the UI
     *
     * @param items The {@link List<PlacesObj>} items
     */
    public void update(List<PlacesObj> items){
        this.items = items;
        notifyDataSetChanged();
    }

    public String getPhotosParams(){
        String apiKey = MapsApplication.getInstance().getApplicationContext().getResources().getString(R.string.google_maps_api_key);
        return "&key="+apiKey+"&maxwidth=400";
    }
}
