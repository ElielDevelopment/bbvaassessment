package com.development.eliel.bbvaassessment.presenters.impl;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.development.eliel.assessmentcore.model.PlacesObj;
import com.development.eliel.bbvaassessment.R;
import com.development.eliel.bbvaassessment.presenters.DetailPresenter;
import com.development.eliel.bbvaassessment.presenters.base.BasePresenterImpl;
import com.development.eliel.bbvaassessment.views.ViewEvent;
import com.development.eliel.bbvaassessment.views.ViewEventType;
import com.development.eliel.bbvaassessment.views.activities.PlaceDetailActivity;

/**
 * Created by eliel.development@gmail.com on 3/29/17.
 */

public class DetailPresenterImpl extends BasePresenterImpl implements DetailPresenter {

    private PlacesObj place;

    @Override
    public void onExtrasReceived(Bundle extras) {
        if(extras != null) {
            if (extras.containsKey(PlaceDetailActivity.KEY_PLACE)) {
                place = (PlacesObj) extras.getSerializable(PlaceDetailActivity.KEY_PLACE);
                ViewEvent viewEvent = new ViewEvent(ViewEventType.PRESENT_OBJECT_EVENT_TYPE);
                viewEvent.getModel().put(ViewEvent.KEY_RESOURCE_ID, R.id.place_detail_container);
                viewEvent.getModel().put(ViewEvent.KEY_ENTRY, place);
                notifyView(viewEvent);
            }
        }
    }

    @Override
    public void onNavigateButtonClicked() {

        Uri gmmIntentUri = Uri.parse("google.navigation:q="+place.getFormatted_address().replace(" ","+")+"");


        ViewEvent viewEvent = new ViewEvent(ViewEventType.OPENAPP_EVENT_TYPE);
        viewEvent.getModel().put(ViewEvent.KEY_APP, "com.google.android.apps.maps");
        viewEvent.getModel().put(ViewEvent.KEY_URI, gmmIntentUri);
        notifyView(viewEvent);
    }
}
