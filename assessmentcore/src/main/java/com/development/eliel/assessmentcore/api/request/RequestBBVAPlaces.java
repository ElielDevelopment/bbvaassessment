package com.development.eliel.assessmentcore.api.request;

import java.io.Serializable;

/**
 * Created by eliel.development@gmail.com on 3/28/17.
 *
 * This class encapsules the params of the Google Place API endpoint to
 * retrieve places nearby a given position
 *
 */

public class RequestBBVAPlaces implements Serializable{
    private String query;
    private Double lat;
    private Double lng;
    private Integer radius;
    private String apiKey;

    public String getQuery() {
        return query;
    }

    public RequestBBVAPlaces withQuery(String query) {
        this.query = query;
        return this;
    }

    public Double getLat() {
        return lat;
    }

    public RequestBBVAPlaces withLat(Double lat) {
        this.lat = lat;
        return this;
    }

    public Double getLng() {
        return lng;
    }

    public RequestBBVAPlaces withLng(Double lng) {
        this.lng = lng;
        return this;
    }

    public Integer getRadius() {
        return radius;
    }

    public RequestBBVAPlaces withRadius(Integer radius) {
        this.radius = radius;
        return this;
    }

    public RequestBBVAPlaces setQuery(String query) {
        this.query = query;
        return this;
    }

    public RequestBBVAPlaces setLat(Double lat) {
        this.lat = lat;
        return this;
    }

    public RequestBBVAPlaces setLng(Double lng) {
        this.lng = lng;
        return this;
    }

    public RequestBBVAPlaces setRadius(Integer radius) {
        this.radius = radius;
        return this;
    }

    public String getApiKey() {
        return apiKey;
    }

    public RequestBBVAPlaces setApiKey(String apiKey) {
        this.apiKey = apiKey;
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RequestBBVAPlaces{");
        sb.append("query='").append(query).append('\'');
        sb.append(", lat=").append(lat);
        sb.append(", lng=").append(lng);
        sb.append(", radius=").append(radius);
        sb.append(", apiKey='").append(apiKey).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
