package com.development.eliel.assessmentcore.api.client;

/**
 * Created by eliel.development@gmail.com on 3/27/17.
 *
 * This class is a Generic Response representation, allows to get a
 * HTTP Response with a Generic body which is in fact the response of
 * the Web Service
 *
 */

public class Response<T> {
    private int code;
    private T body;

    public void setBody(T body) { this.body = body; }
    public T body() { return body; }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Response{");
        sb.append("code=").append(code);
        sb.append(", body=").append(body);
        sb.append('}');
        return sb.toString();
    }
}
