package com.development.eliel.assessmentcore.api.client;

/**
 * Created by eliel.development@gmail.com on 3/27/17.
 *
 * A simple Error representation class
 *
 */

public class Error extends Exception{
    private int code;
    private String message;

    public Error(String detailMessage, int code){
        this.message = detailMessage;
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Error{");
        sb.append("code=").append(code);
        sb.append(", message='").append(message).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
