package com.development.eliel.assessmentcore.api.responses;

import com.development.eliel.assessmentcore.model.PlacesObj;

import java.io.Serializable;
import java.util.List;

/**
 * Created by eliel.development@gmail.com on 3/27/17.
 *
 * Representation of the JSON response of the
 * https://maps.googleapis.com/maps/api/place/textsearch/json
 * enpoint
 *
 *
 */

public class SearchPlacesResponse implements Serializable{
    private String next_page_token;

    private List<PlacesObj> results;

    private String status;

    private String error_message;

    public String getNext_page_token() {
        return next_page_token;
    }

    public void setNext_page_token(String next_page_token) {
        this.next_page_token = next_page_token;
    }

    public List<PlacesObj> getResults() {
        return results;
    }

    public void setResults(List<PlacesObj> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SearchPlacesResponse{");
        sb.append("next_page_token='").append(next_page_token).append('\'');
        sb.append(", results=").append(results);
        sb.append(", status='").append(status).append('\'');
        sb.append(", error_message='").append(error_message).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
