package com.development.eliel.assessmentcore.api.client;

import com.development.eliel.assessmentcore.api.client.Error;
import com.development.eliel.assessmentcore.api.client.Response;

/**
 * Created by eliel.development@gmail.com on 3/27/17.
 *
 * Interface to provide methods to handle an Http request
 * {@link T} It's a generic representation of the expected
 * return type
 *
 */

public interface Callback<T> {
    /**
     * Handles the Successful response
     *
     * @param response A {@link Response} object with a {@link T} body
     */
    void onSuccess(Response<T> response);

    /**
     *
     * Handles the Http Error response
     *
     * @param error An {@link Error} object
     */
    void onHttpError(Error error);

    /**
     *
     * Handles the occur of an Exception
     *
     * @param e The {@link Exception} object
     */
    void onFailure(Exception e);
}
