package com.development.eliel.assessmentcore.model;

import java.io.Serializable;

/**
 * Created by eliel.development@gmail.com on 3/28/17.
 */

public class PlacesGeometry implements Serializable{

    private PlacesLocation location;

    private PlacesViewport viewport;

    public PlacesLocation getLocation() {
        return location;
    }

    public void setLocation(PlacesLocation location) {
        this.location = location;
    }

    public PlacesViewport getViewport() {
        return viewport;
    }

    public void setViewport(PlacesViewport viewport) {
        this.viewport = viewport;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PlacesGeometry{");
        sb.append("location=").append(location);
        sb.append(", viewport=").append(viewport);
        sb.append('}');
        return sb.toString();
    }
}
