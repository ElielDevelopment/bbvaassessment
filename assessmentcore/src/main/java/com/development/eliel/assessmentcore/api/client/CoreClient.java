package com.development.eliel.assessmentcore.api.client;

import com.development.eliel.assessmentcore.api.request.RequestBBVAPlaces;
import com.development.eliel.assessmentcore.api.client.base.DefHttpClient;
import com.development.eliel.assessmentcore.api.responses.SearchPlacesResponse;

/**
 * Created by eliel.development@gmail.com on 3/28/17.
 *
 * This client implements {@link DefHttpClient} and provides
 * methods to get Places from Google Places API
 *
 */

public class CoreClient extends DefHttpClient {

    private static final String BASEURL = "https://maps.googleapis.com/maps/api/place/textsearch/json";

    /**
     *
     * Gets a list of Places matching the given Request data
     *
     * @param requestBBVAPlaces {@link RequestBBVAPlaces} instance that includes query, location, radius and API Key
     * @param callback {@link Callback} instance to handle the response
     */
    public void getBBVAPlaces(RequestBBVAPlaces requestBBVAPlaces, Callback<SearchPlacesResponse> callback){
        //"https://maps.googleapis.com/maps/api/place/textsearch/json?query=BBVA+Compass&location=19.2969978,-99.1511504&radius=10000&key=AIzaSyC27EOolqW682yYQzUm_ZAY0DNxbaCg8OQ",
        execute(BASEURL+"?query="+
                        requestBBVAPlaces.getQuery()+
                        "&location=" +
                        requestBBVAPlaces.getLat().toString() +
                        "," +
                        requestBBVAPlaces.getLng().toString() +
                        "&radius=" +
                        requestBBVAPlaces.getRadius().toString() +
//                        "&key=AIzaSyC27EOolqW682yYQzUm_ZAY0DNxbaCg8OQ",
                        "&key=" + requestBBVAPlaces.getApiKey(),

                callback,
                SearchPlacesResponse.class);
    }
}
