package com.development.eliel.assessmentcore.bo;

import com.development.eliel.assessmentcore.api.request.RequestBBVAPlaces;
import com.development.eliel.assessmentcore.api.client.Callback;
import com.development.eliel.assessmentcore.api.client.CoreClient;
import com.development.eliel.assessmentcore.api.responses.SearchPlacesResponse;
import com.development.eliel.assessmentcore.bo.base.BaseBO;

/**
 * Created by eliel.development@gmail.com on 3/27/17.
 *
 * Business Object class that provide access to the Google Places API
 * Methods
 *
 */

public class MapsBO extends BaseBO {

    public MapsBO(CoreClient client){
        setClient(client);
    }

    /**
     *
     * Gets a list of Places matching the given Request data
     *
     * @param requestBBVAPlaces {@link RequestBBVAPlaces} instance that includes query, location, radius and API Key
     * @param callback {@link Callback} instance to handle the response
     */
    public void getBBVAPlaces(RequestBBVAPlaces requestBBVAPlaces, Callback<SearchPlacesResponse> callback){
        client().getBBVAPlaces(requestBBVAPlaces, callback);
    }

    /**
     *
     * Retrieves a {@link CoreClient} instance
     * from the {@link BaseBO} getClient() Method
     *
     * @return The {@link CoreClient} instance
     */
    private CoreClient client(){
        return (CoreClient) getClient();
    }

}
