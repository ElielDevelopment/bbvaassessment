package com.development.eliel.assessmentcore.api.client;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by eliel.development@gmail.com on 3/28/17.
 *
 * This {@link AsyncTask} creates an {@link HttpsURLConnection}
 * and gets the response as a stream that will be cast to a generic
 * object {@link T}
 *
 */

public class DownloadTask<T> extends AsyncTask<String, Void, Response<T>> {

    private static final String TAG = DownloadTask.class.getSimpleName();
    private Callback<T> mCallback;
    private Class<T> mType;
    private String url;


    public DownloadTask(Callback<T> callback, Class<T> type){
        mCallback = callback;
        mType = type;
    }

    public void excecute(String url){
        this.url = url;
        executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    protected Response<T> doInBackground(String... strings) {

        try {
            String responseStr  = null;

            responseStr = get(new URL(url));

            Log.d(TAG, responseStr);

            JSONObject json = new JSONObject(responseStr);

            T t= createT(mType, json);

            Response<T> response = new Response<T>();
            response.setBody(t);



            return response;

        } catch (JSONException | IOException e) {
            e.printStackTrace();
            mCallback.onFailure(e);
        } catch (InstantiationException e) {
            mCallback.onFailure(e);
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            mCallback.onFailure(e);
            e.printStackTrace();
        } catch (Error error) {
            mCallback.onHttpError(error);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Response<T> s) {
        super.onPostExecute(s);
        if (s != null)
            mCallback.onSuccess(s);

    }


    /**
     *
     * Executes an HTTP GET petition for the given Url
     *
     * @param url The endpoint to hit
     * @return The response as {@link String}
     * @throws IOException
     * @throws Error
     */
    private String get(URL url) throws IOException, Error {
        InputStream stream = null;
        HttpsURLConnection connection = null;
        String result = null;
        try {
            connection = (HttpsURLConnection) url.openConnection();
            connection.setReadTimeout(15000);
            connection.setConnectTimeout(15000);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();
            int responseCode = connection.getResponseCode();
            if (responseCode != HttpsURLConnection.HTTP_OK) {
                throw new Error("An error occurred, please try again later.", responseCode);
            }
            stream = connection.getInputStream();
            if (stream != null) {
                result = readStream(stream);
            }
        } finally {
            // Close Stream and disconnect HTTPS connection.
            if (stream != null) {
                stream.close();
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return result;
    }

    /**
     *
     * Reads an {@link InputStream} and return a String
     *
     * @param stream The {@link InputStream} object
     * @return {@link String} value of the stream
     * @throws IOException
     */
    private String readStream(InputStream stream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
        StringBuilder sb = new StringBuilder();
        String line = null;

        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
            }
        }

        return sb.toString();


    }


    /**
     *
     * Transforms a {@link JSONArray} objecg into a List of {@link T} generic
     * Objects
     *
     * @param forClass The expected type of the Objects
     * @param array The {@link JSONArray} object
     * @param <T> The Generic type
     * @return A List of {@link T} objects
     * @throws JSONException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public  static <T> ArrayList<T> getJSONObjectListFromJSONArray(Class<T> forClass, JSONArray array)
            throws JSONException, InstantiationException, IllegalAccessException {
        ArrayList<T> tObjects = new ArrayList<>();
        for (int i = 0;
             i < (array != null ? array.length() : 0);
             tObjects.add( (T) createT(forClass, array.getJSONObject(i++)))
                );
        return tObjects;
    }

    /**
     *
     * Creates a Generic object using Reflection
     *
     * @param forClass The expected output object class
     * @param jObject The input {@link JSONObject} object
     * @param <T> The Generic type of the expected output
     * @return A generic {@link T} object
     * @throws JSONException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private static <T> T createT(Class<T> forClass, JSONObject jObject) throws JSONException, IllegalAccessException, InstantiationException {
        T tObject = forClass.newInstance();
        for (Field field : tObject.getClass().getDeclaredFields()) {
            field.setAccessible(true);

            if (jObject.has(field.getName())) {

                if (jObject.get(field.getName()) instanceof JSONArray) {
                    ParameterizedType stringListType = (ParameterizedType) field.getGenericType();
                    field.set(tObject, getJSONObjectListFromJSONArray((Class<?>) stringListType.getActualTypeArguments()[0], (JSONArray) jObject.get(field.getName())));
                } else if (jObject.get(field.getName()) instanceof JSONObject) {
                    field.set(tObject, createT(field.getType(), (JSONObject) jObject.get(field.getName())));
                } else {
                    field.set(tObject, jObject.get(field.getName()));
                }
            }
        }

        return tObject;
    }





}



