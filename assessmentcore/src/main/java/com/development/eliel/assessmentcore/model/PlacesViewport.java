package com.development.eliel.assessmentcore.model;

import java.io.Serializable;

/**
 * Created by eliel.development@gmail.com on 3/29/17.
 */

public class PlacesViewport implements Serializable{
    private PlacesLocation northeast;
    private PlacesLocation southwest;

    public PlacesLocation getNortheast() {
        return northeast;
    }

    public void setNortheast(PlacesLocation northeast) {
        this.northeast = northeast;
    }

    public PlacesLocation getSouthwest() {
        return southwest;
    }

    public void setSouthwest(PlacesLocation southwest) {
        this.southwest = southwest;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PlacesViewport{");
        sb.append("northeast=").append(northeast);
        sb.append(", southwest=").append(southwest);
        sb.append('}');
        return sb.toString();
    }
}
