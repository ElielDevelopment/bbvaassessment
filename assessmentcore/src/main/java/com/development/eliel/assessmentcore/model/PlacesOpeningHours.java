package com.development.eliel.assessmentcore.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eliel.development@gmail.com on 3/28/17.
 */

public class PlacesOpeningHours implements Serializable{
    private Boolean open_now;


    public Boolean getOpen_now() {
        return open_now;
    }

    public void setOpen_now(Boolean open_now) {
        this.open_now = open_now;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PlacesOpeningHours{");
        sb.append("open_now=").append(open_now);
        sb.append('}');
        return sb.toString();
    }
}
