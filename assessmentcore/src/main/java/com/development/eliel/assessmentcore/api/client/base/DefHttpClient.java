package com.development.eliel.assessmentcore.api.client.base;

import com.development.eliel.assessmentcore.api.client.Callback;
import com.development.eliel.assessmentcore.api.client.DownloadTask;

/**
 * Created by eliel.development@gmail.com on 3/27/17.
 * Default Http Client to execute http requests
 *
 */

public class DefHttpClient {

    /**
     *
     * Executes a Http request for the given Url
     *
     * @param url The endpoint to reach
     * @param callback {@link Callback} interface implementation to handle the results
     * @param type The expected Object type to return
     */
    protected void execute(String url, Callback<?> callback, Class<?> type){
        new DownloadTask(callback, type).excecute(url);
    }



}
