package com.development.eliel.assessmentcore.model;

import java.io.Serializable;

/**
 * Created by eliel.development@gmail.com on 3/28/17.
 */

public class PlacesPhotos implements Serializable{
    private Integer height;
    private String photo_reference;
    private Integer width;

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getPhoto_reference() {
        return photo_reference;
    }

    public void setPhoto_reference(String photo_reference) {
        this.photo_reference = photo_reference;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PlacesPhotos{");
        sb.append("height=").append(height);
        sb.append(", photo_reference='").append(photo_reference).append('\'');
        sb.append(", width=").append(width);
        sb.append('}');
        return sb.toString();
    }
}
