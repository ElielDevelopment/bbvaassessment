package com.development.eliel.assessmentcore.api;

import com.development.eliel.assessmentcore.api.client.CoreClient;
import com.development.eliel.assessmentcore.bo.MapsBO;

/**
 * Created by eliel.development@gmail.com on 3/27/17.
 *
 * This class encapsules the {@link CoreClient} and it's
 * Business Objects to provide access to their methods
 *
 */

public class ApiClient {

    private static ApiClient instance;
    private static CoreClient client;

    private MapsBO mapsBO;

    /**
     *
     * Provides access to the {@link ApiClient} instance
     *
     * @return the {@link ApiClient} instance
     */
    public static ApiClient getInstance(){
        if (instance == null){
            instance = new ApiClient();
            client = new CoreClient();
        }

        return instance;
    }


    /**
     *
     * Provides access to the {@link MapsBO} instance
     *
     * @return the {@link MapsBO} instance
     */
    public MapsBO maps(){
        if (mapsBO == null){
            mapsBO = new MapsBO(client);
        }

        return mapsBO;
    }

}
