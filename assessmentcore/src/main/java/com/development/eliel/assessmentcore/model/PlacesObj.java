package com.development.eliel.assessmentcore.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by eliel.development@gmail.com on 3/28/17.
 */

public class PlacesObj implements Serializable{
    private String formatted_address;
    private PlacesGeometry geometry;
    private String icon;
    private String id;
    private String name;
    private PlacesOpeningHours opening_hours;
    private ArrayList<PlacesPhotos> photos;
    private String place_id;
    private Object rating;
    private String reference;
    /*private List<String> types;*/

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public PlacesGeometry getGeometry() {
        return geometry;
    }

    public void setGeometry(PlacesGeometry geometry) {
        this.geometry = geometry;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PlacesOpeningHours getOpening_hours() {
        return opening_hours;
    }

    public void setOpening_hours(PlacesOpeningHours opening_hours) {
        this.opening_hours = opening_hours;
    }

    public ArrayList<PlacesPhotos> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<PlacesPhotos> photos) {
        this.photos = photos;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public Object getRating() {
        return rating;
    }

    public void setRating(Object rating) {
        this.rating = rating;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    /*public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }*/

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PlacesObj{");
        sb.append("formatted_address='").append(formatted_address).append('\'');
        sb.append(", geometry=").append(geometry);
        sb.append(", icon='").append(icon).append('\'');
        sb.append(", id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", opening_hours=").append(opening_hours);
        sb.append(", photos=").append(photos);
        sb.append(", place_id='").append(place_id).append('\'');
        sb.append(", rating=").append(rating);
        sb.append(", reference='").append(reference).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
