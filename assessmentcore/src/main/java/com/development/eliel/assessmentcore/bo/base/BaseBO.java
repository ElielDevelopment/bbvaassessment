package com.development.eliel.assessmentcore.bo.base;

import com.development.eliel.assessmentcore.api.client.base.DefHttpClient;

/**
 * Created by eliel.development@gmail.com on 3/27/17.
 *
 * This is the Base class from which all the Business Objects has to extends
 * since it provides access to the {@link DefHttpClient} that could be Casted
 * to a particular implementation of it.
 *
 */

public class BaseBO {

    private DefHttpClient client;

    protected DefHttpClient getClient() {
        return client;
    }

    protected void setClient(DefHttpClient client) {
        this.client = client;
    }
}
